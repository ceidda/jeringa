package foo.cei.reflexion.jeringa;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class JeringaInjector {

	private static JeringaInjector instance;

	public static JeringaInjector getInstance() {
		if (JeringaInjector.instance == null) {
			JeringaInjector.instance = new JeringaInjector();
		}
		return instance;
	}

	private JeringaInjector() {

	}

	public void inyectar(Object object) throws ClassNotFoundException, NoSuchMethodException, SecurityException,
			InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Class<?> clazz = object.getClass();
		Field[] fields = clazz.getDeclaredFields();
		for (Field f : fields) {
			System.out.println(f.getName());
			Annotation[] annotations = f.getDeclaredAnnotations();
			for (Annotation annotation : annotations) {
				System.out.println(annotation.annotationType());
				if (annotation instanceof Jeringa) {
					System.out.println("wiiiiii");
					Jeringa jeringa = (Jeringa) annotation;
					String value = jeringa.value();
					System.out.println(value);
					Class<?> newClazz = Class.forName(value);
					Constructor<?> ctor = newClazz.getConstructor();
					Object newObject = ctor.newInstance();
					System.out.println(newObject);
					f.setAccessible(true);
					f.set(object, newObject);
					f.setAccessible(false);
				}
			}
		}
	}
}
