package foo.cei.reflexion.jeringa.mocks;

import foo.cei.reflexion.jeringa.Jeringa;

public class Foo {

	@TestAnnotation("")
	@Jeringa("foo.cei.reflexion.jeringa.mocks.BarImpl")
	private Bar bar;
	private String name;

	/**
	 * @return the bar
	 */
	public Bar getBar() {
		return bar;
	}

	/**
	 * @param bar
	 *            the bar to set
	 */
	public void setBar(Bar bar) {
		this.bar = bar;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

}
