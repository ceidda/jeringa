package foo.cei.reflexion.jeringa;

import org.junit.Test;

import foo.cei.reflexion.jeringa.mocks.Foo;

import static org.junit.Assert.assertNotNull;

import java.lang.reflect.InvocationTargetException;

public class JeringaInjectorTest {

	@Test
	public void singlentonInstanceTest() {
		JeringaInjector jeringaInjector = JeringaInjector.getInstance();
		assertNotNull(jeringaInjector);
	}
	
	@Test
	public void inyectarTest() throws ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		JeringaInjector jeringaInjector = JeringaInjector.getInstance();
		Foo foo = new Foo();
		jeringaInjector.inyectar(foo);
		assertNotNull(foo.getBar());
	}
}
